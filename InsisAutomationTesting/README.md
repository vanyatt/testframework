#Prerequizites
You need to have JDK on your machine and Maven if no IDE is used.

# How to setup your IDE
## Intelij
Open the project in IntelliJ by New->Project from Existing Sources-> Navigate to project and then select Import project from external model -> Maven, follow the wizzard and select your path to the JDK (preferred version 1.8)
In File/Settings/Plugins install JBehave Support

## Eclipse
Open the project in Eclipse by File -> Open Projects form File System -> At Import Source navigate to project checkout folder (Import as Maven project should appear) -> Click Finish

# How to run the tests
Build the project to see if compilation errors occur. If not in InsisAutomationTesting\src\test\java\com\fadata\jbehave\ui\runners\JUnitStoryRunner.java is the runner used for test execution. Right click on it and from the context menu select Run 'JUnitStoryRunner'.

# Project structure
All configurations and utils are located in InsisAutomationTesting\src\main, while tests, runners and step definitions are located in InsisAutomationTesting\src\test. Tests (in JBehave terms Stories) are located in located in InsisAutomationTesting\src\test\resources while supporting definitions are in InsisAutomationTesting\src\test\java

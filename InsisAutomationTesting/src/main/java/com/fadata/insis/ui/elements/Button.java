package com.fadata.insis.ui.elements;

public class Button extends BaseElement {
	

	public void clickById(String id) {
		findElementById(id).click();
	}
	
	public void clickByXpath(String xpath) {
		findElementByXpath(xpath).click();
	}
}

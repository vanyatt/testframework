package com.fadata.insis.ui.util;

import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;

import io.github.bonigarcia.wdm.ChromeDriverManager;
import io.github.bonigarcia.wdm.FirefoxDriverManager;

class WebDriverManager {
	
	public enum WebDriverManagerEnum {
		INSTANCE;

		private WebDriver driver = setupBrowser();
		
		WebDriver getDriver() {
			return driver;
		}
		
		void tearDown() {
			// TODO: refactor
			if (driver != null) {
				driver.quit();	
			}
		}
		
		private WebDriver setupBrowser() {
			Properties loginConfiguration = ArgusUtils.getConfigProperties();
			String browser = System.getProperty("default.browser") == null ? loginConfiguration.getProperty("default.browser") : null;
			
			switch(Constants.Browsers.valueOf(browser)) {
				case firefox : 
					FirefoxDriverManager.getInstance().setup();
					WebDriver driver = new FirefoxDriver();
					driver.manage().window().maximize();
			        return driver;
				case chrome : 
					ChromeDriverManager.getInstance().setup();
					ChromeOptions options = new ChromeOptions();
                    options.addArguments("--start-maximized");
			        return new ChromeDriver(options);
				default:
					// TODO: throw exception
					return null;
			}
		}
	}
}

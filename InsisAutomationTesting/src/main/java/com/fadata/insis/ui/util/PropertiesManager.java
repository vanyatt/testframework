package com.fadata.insis.ui.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;


class PropertiesManager {

	public enum PropertiesManagerEnum {
		INSTANCE;

		Properties configProperties = loadConfig();
		Properties elementsMappingProperties = loadElementsMapping();

		/**
		 * Getter for Configuration Properties
		 * @return
		 */
		Properties getConfigProperties() {
			return configProperties;
		}
		
		/**
		 * Getter for Elements Mapping Properties
		 * @return
		 */
		Properties getElementsMappingProperties() {
			return elementsMappingProperties;
		}
		
		/**
		 * Loads all of the project's properties taken from {@link Constants.INSIS_CONFIG_PROPERTIES_FILE_PATH}
		 */
		private static Properties loadConfig(){
			return loadProperties(Constants.INSIS_CONFIG_PROPERTIES_FILE_PATH);
	    }

		/**
		 * Load Components Id Mapping from properties file
		 * @return
		 */
	    private static Properties loadElementsMapping(){
			return loadProperties(Constants.INSIS_ELEMENTS_MAPPING_PROPERTIES_FILE_PATH);
	    }
	    
	    private static Properties loadProperties(String url){
	    	Properties props = new Properties();
	        try {
	        	props.load(new FileInputStream(url));
	        } catch (IOException ex) {
	            ex.printStackTrace();
	        }
	        return props;
	    }
	}
}

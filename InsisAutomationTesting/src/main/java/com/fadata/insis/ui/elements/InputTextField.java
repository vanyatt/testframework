package com.fadata.insis.ui.elements;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import com.fadata.insis.ui.util.ArgusUtils;

public class InputTextField extends BaseElement {

    private void enterValueInWebElement(WebElement textField, String value, String key) {
        textField.click();

        if (key != null) {
            String uppercase = key.toUpperCase();
            textField.sendKeys(value, Keys.valueOf(uppercase));
        } else {
            textField.sendKeys(value);
        }
    }

    private WebElement findTextFieldById(String id) {
        return findElementByXpath("//input[contains(@id, ':" + id + ":')]");
    }

	public void enterValue(WebElement element, String value){
        enterValueInWebElement(element,value, null);
	}

    public void enterValueAndPressKey(WebElement element, String value, String key){
        enterValueInWebElement(element,value, key);
    }

	public String getValue(WebElement element){
	    return element.getText();
    }
}

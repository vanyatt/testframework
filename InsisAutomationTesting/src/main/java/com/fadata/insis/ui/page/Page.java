package com.fadata.insis.ui.page;

import com.fadata.insis.ui.elements.*;
import com.fadata.insis.ui.util.ArgusUtils;
import org.openqa.selenium.WebDriver;

public class Page {

    private WebDriver driver;

	private Button button;
	private InputTextField textField;

	private static Page instance = null;
		
	public static Page getInstance() {
		if (instance == null) {
			instance = new Page();
		}
		
		return instance;
	}


	
	protected Page() {
	    this.driver = ArgusUtils.getWebDriver();
        this.button = new Button();
        this.textField = new InputTextField();
	}

	public void openHomePage(){
	    driver.get("https://www.google.bg");
    }

	public void clickButton(String name) {
		button.clickByXpath(ArgusUtils.getElementsMappingProperties().getProperty(name));
	}

	public void enterValueInInputTextField(String xpath, String value) {
		textField.enterValue(textField.findElementByXpath(xpath),value);
	}

	
	//TODO: custom temporary code that is used for one specific row selection. To be reviewed in later stages
	public void click(String xpath) {
		button.findElementByXpath(xpath).click();
	}


	public void typeInField(String fieldName, String value){
		textField.enterValue(textField.findElementByXpath(fieldName),value);
	}

}
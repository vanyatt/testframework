package com.fadata.insis.ui.elements;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import com.fadata.insis.ui.util.ArgusUtils;

abstract class BaseElement {
	
	WebDriver driver = ArgusUtils.getWebDriver();
    Actions action = new Actions(driver);

	public WebElement findElementByClassName(String className) {
		return driver.findElement(By.className(className));
	}

    public WebElement findElementByCSSSelector(String cssSelector) {
		return driver.findElement(By.cssSelector(cssSelector));
	}

    public WebElement findElementById(String id) {
		return driver.findElement(By.id(id));
	}

    public WebElement findElementByLinkText(String linkText) {
		return driver.findElement(By.linkText(linkText));
	}
	
	public WebElement findElementByName(String name) {
		return driver.findElement(By.name(name));
	}

    public WebElement findElementByPartialLinkText(String partialLinkText) {
		return driver.findElement(By.partialLinkText(partialLinkText));
	}

    public WebElement findElementByTagName(String tagName) {
		return driver.findElement(By.tagName(tagName));
	}
	
	public WebElement findElementByXpath(String xpath) {
		return driver.findElement(By.xpath(xpath));
	}

    public WebElement findElementByAttribute(String element,String attribute, String value) {
        return driver.findElement(By.cssSelector(element + "[" + attribute + "='" + value + "']"));
    }

}

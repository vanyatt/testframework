package com.fadata.insis.ui.util;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

public class ArgusUtils {
	
	/**
	 * Returns the project's WebDriver
	 */
	public static WebDriver getWebDriver() {
		return WebDriverManager.WebDriverManagerEnum.INSTANCE.getDriver();
	}

	/**
	 * Return all properties read from the project's configuration file: config.properties
	 */
	public static Properties getConfigProperties() {
		return PropertiesManager.PropertiesManagerEnum.INSTANCE.getConfigProperties();
	}
	
	/**
	 * Return all properties read from the project's configuration file: components_mapping.properties
	 */
	public static Properties getElementsMappingProperties() {
		return PropertiesManager.PropertiesManagerEnum.INSTANCE.getElementsMappingProperties();
	}

}

package com.fadata.insis.ui.util;

public class Constants {
	
	public static enum Browsers {
		firefox, chrome
	}
	
	public static final int WEB_DRIVER_WAIT_TIME_OUT_IN_SECONDS = 30;
	
	// files and folders positioning
	public static final String INSIS_CONFIG_PROPERTIES_FILE_PATH = "src/main/resources/config.properties";
	public static final String INSIS_ELEMENTS_MAPPING_PROPERTIES_FILE_PATH = "src/main/resources/components_mapping_partial_ids.properties";

}

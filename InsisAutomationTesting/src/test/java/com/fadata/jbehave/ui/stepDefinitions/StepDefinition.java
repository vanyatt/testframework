package com.fadata.jbehave.ui.stepDefinitions;

import org.jbehave.core.annotations.*;

import com.fadata.insis.ui.page.Page;

public class StepDefinition {

    Page page;

    public StepDefinition() {
    	this.page = Page.getInstance();
    }
    

    @BeforeStories
    public void beforeStories(){
        page.openHomePage();
    }
    
    @Given("Click on $name button")
    @When("Click on $name button")
    @Then("Click on $name button")
    public void clickButton(String name){
    	page.clickButton(name);
    }

}
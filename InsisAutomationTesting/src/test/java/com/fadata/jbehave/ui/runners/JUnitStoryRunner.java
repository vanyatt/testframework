package com.fadata.jbehave.ui.runners;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.jbehave.core.configuration.Configuration;
import org.jbehave.core.configuration.MostUsefulConfiguration;
import org.jbehave.core.embedder.Embedder;
import org.jbehave.core.embedder.EmbedderControls;
import org.jbehave.core.io.CodeLocations;
import org.jbehave.core.io.LoadFromClasspath;
import org.jbehave.core.io.StoryFinder;
import org.jbehave.core.junit.JUnitStories;
import org.jbehave.core.reporters.Format;
import org.jbehave.core.reporters.StoryReporterBuilder;
import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;
import org.jbehave.core.steps.PrintStreamStepMonitor;
import org.junit.runner.RunWith;

import com.fadata.insis.ui.util.ArgusUtils;
import com.fadata.jbehave.ui.stepDefinitions.StepDefinition;
import com.github.valfirst.jbehave.junit.monitoring.JUnitReportingRunner;

@RunWith(JUnitReportingRunner.class)
public class JUnitStoryRunner extends JUnitStories {

	Properties config = null;
	
	public JUnitStoryRunner() {
		super();
		Embedder embedder = configuredEmbedder();
		embedder.useMetaFilters(Arrays.asList("-skip"));
		
		EmbedderControls embedderControls = embedder.embedderControls();
		embedderControls.doIgnoreFailureInStories(false);
		embedderControls.doIgnoreFailureInView(false);
		
	}
	
	@Override
	public Configuration configuration() {
		// TODO: impelement at some point when we decide how to import the test files on bamboo
//		loadAllFiles();
		
		config = ArgusUtils.getConfigProperties();
		
		if (config == null) {
			throw new RuntimeException("Cannot find INSIS configuration file");
		}
		
		return new MostUsefulConfiguration().useStoryLoader(new LoadFromClasspath(this.getClass().getClassLoader()))
				.useStepMonitor(new PrintStreamStepMonitor())
				.useStoryReporterBuilder(new StoryReporterBuilder().withDefaultFormats()
						.withFormats(Format.HTML, Format.CONSOLE, Format.TXT, Format.XML));
	}

	@Override
	public InjectableStepsFactory stepsFactory() {
		return new InstanceStepsFactory(configuration(), new StepDefinition());
	}

	@Override
	protected List<String> storyPaths() {
		return new StoryFinder().findPaths(CodeLocations.codeLocationFromClass(this.getClass()), Arrays.asList("**/*.story"), Arrays.asList(""));
	}

	private void loadAllFiles() {
		File source = new File("all files path location");
		File dest = new File("src/test/resources/ui/stories/policy");
		
		try {
		    FileUtils.copyDirectory(source, dest);
		} catch (IOException e) {
		    e.printStackTrace();
		}
	}
}
